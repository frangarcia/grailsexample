package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import spock.lang.Ignore
import spock.lang.Unroll
import org.springframework.dao.DataIntegrityViolationException


class AdministratorControllerUnitSpec extends ControllerSpec {

    def setup() {
        User.metaClass.encodePassword { "encoded" }
        User.metaClass.isDirty { false }

        controller.metaClass.message { args -> 'message' }
    }

    def 'When visiting the index method, the application redirects to the list method'(){
        when:
            controller.index()
        then:
            redirectArgs.action == 'list'
    }

    def 'The method create return an instance of an administrator'(){
        given:
            controller.params.username = "frangarcia"
        when:
            def model = controller.create()
        then:
            model.administratorInstance.username == "frangarcia"
    }

    def 'If an error occurs when saving the administrator, the create view is rendered again'(){
        given:
            mockDomain(Administrator)

        when:
            controller.params.username = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new administrator'(){
        given:
            mockDomain(Administrator)

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "frangarcia"
            controller.params.password = "password"
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an administrator, if does not exist, the app redirect to the list of administrator'() {
        given:
            mockDomain(Administrator)

        when:
            controller.params.id = 123
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an administrator, if exist, the app returns the instance of the administrator'() {
        given:
            def administratorInstance = new Administrator(id: 1)
        and:
            mockDomain(Administrator, [administratorInstance])

        when:
            def model = controller.show(administratorInstance.id)
        then:
            model.administratorInstance.id == administratorInstance.id
    }

    def 'The method edit return an instance of an administrator if exists'(){
        given:
            def administratorInstance = new Administrator(id: 1)
        and:
            mockDomain(Administrator, [administratorInstance])

        when:
            def model = controller.edit(administratorInstance.id)
        then:
            model.administratorInstance.id == administratorInstance.id
    }

    def 'The method edit redirect to the list action if the administrator does not exists'(){
        given:
            mockDomain(Administrator)
        when:
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'The method update redirect to the list action if the administrator does not exists'(){
        given:
            mockDomain(Administrator)
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If somebody updates the administrator before, it renders again the original object'(){
        given:
            def administratorInstance = new Administrator()
        and:
            mockDomain(Administrator, [administratorInstance])
        and:
            administratorInstance.metaClass.getVersion { 2 }

        when:
            def model = controller.update(administratorInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.administratorInstance.id == administratorInstance.id
    }

    def 'If there is an error updating the administrator, the app renders again the edit view'(){
        given:
            def administratorInstance = new Administrator()
        and:
            mockDomain(Administrator, [administratorInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = null
            controller.params.password = "password"
            def model = controller.update(administratorInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.administratorInstance.id == administratorInstance.id
    }

    def 'If there are no error updating the administrator, the app shows the administrator and shows a message'(){
        given:
            def administratorInstance = new Administrator()
        and:
            mockDomain(Administrator, [administratorInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "username"
            controller.params.password = "password"
            def model = controller.update(administratorInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == administratorInstance.id
    }

    def 'The method delete redirect to the list action if the administrator does not exists'(){
        given:
            mockDomain(Administrator)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If deleting an exception is thrown, the app redirects to show the administrator and shows a message'(){
        given:
            def administratorInstance = new Administrator(id:1)
        and:
            mockDomain(Administrator, [administratorInstance])
            administratorInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }

        when:
            controller.delete(administratorInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
            redirectArgs.id == administratorInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def administratorInstance = new Administrator(id:1)
        and:
            mockDomain(Administrator, [administratorInstance])

        when:
            controller.delete(administratorInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }
}

package com.odobo.eschool

import spock.lang.Unroll
import grails.plugin.spock.UnitSpec

class TeacherUnitSpec extends UnitSpec {

    def setup() {

    }

    @Unroll
    def 'Checking if a teacher teachers in a student group'(){
        given:
            def teacher = new Teacher()
            def studentGroup1 = new StudentGroup()
            def studentGroup2 = new StudentGroup()
            def studentGroupTeacher = new StudentGroupTeacher(teacher: teacher, studentGroup: studentGroup1, tutor: tutor)
            mockDomain(StudentGroupTeacher, [studentGroupTeacher])

        expect:
            teacher.teachesStudentGroup(studentGroup1)
            !teacher.teachesStudentGroup(studentGroup2)

        where:
            tutor << [true, false]
    }

    @Unroll
    def 'Getting the studentGroups of a teacher'(){
        given:
            def teacher = new Teacher()
            def studentGroup1 = new StudentGroup()
            def studentGroup2 = new StudentGroup()
            def studentGroupTeacher1 = new StudentGroupTeacher(teacher: teacher, studentGroup: studentGroup1, tutor: tutor)
            def studentGroupTeacher2 = new StudentGroupTeacher(teacher: teacher, studentGroup: studentGroup2, tutor: tutor)
            mockDomain(StudentGroupTeacher, [studentGroupTeacher1, studentGroupTeacher2])

        expect:
            teacher.studentGroups.containsAll([studentGroup1, studentGroup2])
        where:
            tutor << [true, false]
    }
}

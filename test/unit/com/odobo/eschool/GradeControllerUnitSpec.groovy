package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.SpringSecurityService
import spock.lang.Unroll

class GradeControllerUnitSpec extends ControllerSpec {

    def setup() {
        controller.metaClass.message { args -> 'message' }
    }

    def 'The method create return an instance of an grade'(){
        given:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.grade = 8
        when:
            def model = controller.create(studentGroupInstance.id)
        then:
            model.gradeInstance.grade == 8
    }

    def 'If an error occurs when saving the grade, the create view is rendered again'(){
        given:
            mockDomain(Grade)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.params.grade = null
            controller.params.studentGroupInstance = studentGroupInstance
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new grade'(){
        given:
            mockDomain(Grade)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])

        when:
            controller.params.grade = 8
            controller.params.course = courseInstance
            controller.params.semester = Semester.FIRST_SEMESTER
            controller.params.student = studentInstance
            controller.params.studentGroupInstance = studentGroupInstance
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing a grade, if does not exist, the app redirect to the list of grade'() {
        given:
            mockDomain(Grade)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id

        when:
            controller.show(studentGroupInstance.id)
        then:
            redirectArgs.action == "showGradesByStudentAndStudentGroup"
            redirectArgs.id == studentGroupInstance.id
    }

    def 'When showing an grade, if exist, the app returns the instance of the grade'() {
        given:
            def gradeInstance = new Grade(id: 1)
            mockDomain(Grade, [gradeInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id
        when:
            def model = controller.show(gradeInstance.id)
        then:
            model.gradeInstance.id == gradeInstance.id
            model.studentGroupInstance == studentGroupInstance
    }

    def 'The method edit return an instance of an grade if exists'(){
        given:
            def gradeInstance = new Grade(id: 1)
            mockDomain(Grade, [gradeInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id

        when:
            def model = controller.edit(gradeInstance.id)
        then:
            model.gradeInstance.id == gradeInstance.id
            model.studentGroupInstance == studentGroupInstance
    }

    def 'The method edit redirect to the list action if the grade does not exists'(){
        given:
            mockDomain(Grade)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.params.studentGroupId = studentGroupInstance.id
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showGradesByStudentAndStudentGroup'
            redirectArgs.id == studentGroupInstance.id
    }

    def 'The method update redirect to the showGradesByStudentAndStudentGroup action if the grade does not exists'(){
        given:
            mockDomain(Grade)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupInstance = studentGroupInstance
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showGradesByStudentAndStudentGroup'
    }

    def 'If somebody updates the grade before, it renders again the original object'(){
        given:
            def gradeInstance = new Grade()
            mockDomain(Grade, [gradeInstance])
            gradeInstance.metaClass.getVersion { 2 }
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupInstance = studentGroupInstance
        when:
            def model = controller.update(gradeInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.gradeInstance.id == gradeInstance.id
            renderArgs.model.studentGroupInstance == studentGroupInstance
    }

    def 'If there is an error updating the grade, the app renders again the edit view'(){
        given:
            def gradeInstance = new Grade()
            mockDomain(Grade, [gradeInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupInstance = studentGroupInstance
        when:
            controller.params.grade = 8
            controller.params.course = null
            controller.params.semester = Semester.FIRST_SEMESTER
            controller.params.student = studentInstance
            controller.params.studentGroupInstance = studentGroupInstance
            def model = controller.update(gradeInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.gradeInstance.id == gradeInstance.id
            renderArgs.model.studentGroupInstance == studentGroupInstance
    }

    def 'If there are no errors updating the grade, the app shows the grade and shows a message'(){
        given:
            def gradeInstance = new Grade()
            mockDomain(Grade, [gradeInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])
        and:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupInstance = studentGroupInstance

        when:
            controller.params.grade = 8
            controller.params.course = courseInstance
            controller.params.semester = Semester.FIRST_SEMESTER
            controller.params.student = studentInstance
            controller.params.studentGroupInstance = studentGroupInstance
            def model = controller.update(gradeInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == gradeInstance.id
            redirectArgs.params.studentGroupId == studentGroupInstance.id
    }

    def 'The method delete redirect to the list action if the grade does not exists'(){
        given:
            mockDomain(Grade)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showGradesByStudentAndStudentGroup'
    }

    def 'If deleting an exception is thrown, the app redirects to show the grade and shows a message'(){
        given:
            def gradeInstance = new Grade(id:1)
            mockDomain(Grade, [gradeInstance])
            gradeInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id
        when:
            controller.delete(gradeInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showAbsencesByStudentAndStudentGroup'
            redirectArgs.id == studentGroupInstance.id
    }

    def 'If deleting is successful, the app redirects to listGrades and shows a message'(){
        given:
            def gradeInstance = new Grade(id:1)
            mockDomain(Grade, [gradeInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id
        when:
            controller.delete(gradeInstance.id)
        then:
            renderArgs.template == 'listGrades'
            renderArgs.model.studentGroupInstance == studentGroupInstance
    }

    @Unroll
    def 'Showing grades by student and student group checks if the teacher is a tutor'(){
        given:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            def teacherInstance = new Teacher()
            Teacher.metaClass.amITutor { StudentGroup sg -> amITutor }
            mockDomain(Teacher, [teacherInstance])

        and:
            def springSecurityService = new SpringSecurityService()
            springSecurityService.metaClass.getCurrentUser { teacherInstance }
            controller.springSecurityService = springSecurityService

        when:
            def model = controller.showGradesByStudentAndStudentGroup(studentGroupInstance.id)
        then:
            model.studentGroupInstance == studentGroupInstance
            model.amITutor == amITutor

        where:
            amITutor << [true, false]

    }
}

package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException

class AbsenceControllerUnitSpec extends ControllerSpec {

    def setup() {
        controller.metaClass.message { args -> 'message' }
    }

    def 'The method create return an instance of an absence'(){
        given:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])
        and:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])

        and:
            controller.params.date = new Date()
            controller.params.student = studentInstance
            controller.params.course = courseInstance
        when:
            def model = controller.create(studentGroupInstance.id)
        then:
            model.absenceInstance.student == studentInstance
            model.absenceInstance.student == studentInstance
    }

    def 'If an error occurs when saving the absence, the create view is rendered again'(){
        given:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            mockDomain(Absence)

        when:
            controller.params.studentGroupInstance = studentGroupInstance
            controller.params.date = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new absence'(){
        given:
            mockDomain(Absence)
        and:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])

        and:
            controller.params.date = new Date()
            controller.params.student = studentInstance
            controller.params.course = courseInstance
            controller.params.studentGroupInstance = studentGroupInstance

        when:
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an absence, if does not exist, the app redirect to the list of absence'() {
        given:
            mockDomain(Absence)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.params.id = 123
            controller.params.studentGroupInstance = studentGroupInstance
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an absence, if exist, the app returns the instance of the absence'() {
        given:
            def absenceInstance = new Absence(id: 1)
            mockDomain(Absence, [absenceInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupInstance = studentGroupInstance

        when:
            def model = controller.show(absenceInstance.id)
        then:
            model.absenceInstance.id == absenceInstance.id
    }

    def 'The method delete redirect to the list action if the absence does not exists'(){
        given:
            mockDomain(Absence)
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id

        when:
            controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showAbsencesByStudentAndStudentGroup'
            redirectArgs.id == studentGroupInstance.id
    }

    def 'If deleting an exception is thrown, the app redirects to show the absence and shows a message'(){
        given:
            def absenceInstance = new Absence(id:1)
            mockDomain(Absence, [absenceInstance])
            absenceInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id

        when:
            controller.delete(absenceInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'showAbsencesByStudentAndStudentGroup'
            redirectArgs.id == absenceInstance.id
            redirectArgs.params.studentGroupId == studentGroupInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def absenceInstance = new Absence(id:1)
            mockDomain(Absence, [absenceInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id

        when:
            controller.delete(absenceInstance.id)
        then:
            renderArgs.template == "listAbsences"
            renderArgs.model.studentGroupInstance == studentGroupInstance
    }

    def 'Returning a student group instance when showing the absences'(){
        given:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        when:
            def model = controller.showAbsencesByStudentAndStudentGroup(studentGroupInstance.id)
        then:
            model.studentGroupInstance == studentGroupInstance
    }

    def 'Changing the justifiable field returns a template with the student group instance'(){
        given:
            def absenceInstance = new Absence()
            mockDomain(Absence, [absenceInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            controller.params.studentGroupId = studentGroupInstance.id
        when:
            controller.changeJustifiable(absenceInstance.id)
        then:
            renderArgs.template == "listAbsences"
            renderArgs.model.studentGroupInstance == studentGroupInstance

    }

    def 'Searching returns a template with a student list and a student group'(){
        given:
            def absenceInstance = new Absence()
            mockDomain(Absence, [absenceInstance])
        and:
            def studentGroupInstance = new StudentGroup()
            mockDomain(StudentGroup, [studentGroupInstance])
            def studentInstanceList = [new Student(), new Student()]
            studentGroupInstance.metaClass.searchStudents {name, surnames -> studentInstanceList }

        and:
            controller.params.studentGroupInstance = studentGroupInstance
        when:
            controller.search()
        then:
            renderArgs.template == "searchAbsences"
            renderArgs.model.studentGroupInstance == studentGroupInstance
            renderArgs.model.studentInstanceList == studentInstanceList
    }
}

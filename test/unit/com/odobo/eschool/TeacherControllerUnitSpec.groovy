package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException
import spock.lang.Unroll

class TeacherControllerUnitSpec extends ControllerSpec {

    def setup() {
        User.metaClass.encodePassword { "encoded" }
        User.metaClass.isDirty { false }

        controller.metaClass.message { args -> 'message' }
    }

    def 'When visiting the index method, the application redirects to the list method'(){
        when:
            controller.index()
        then:
            redirectArgs.action == 'list'
    }

    def 'The method create return an instance of an teacher'(){
        given:
            controller.params.username = "frangarcia"
        when:
            def model = controller.create()
        then:
            model.teacherInstance.username == "frangarcia"
    }

    def 'If an error occurs when saving the teacher, the create view is rendered again'(){
        given:
            mockDomain(Teacher)

        when:
            controller.params.username = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new teacher'(){
        given:
            mockDomain(Teacher)

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "frangarcia"
            controller.params.password = "password"
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an teacher, if does not exist, the app redirect to the list of teacher'() {
        given:
            mockDomain(Teacher)

        when:
            controller.params.id = 123
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an teacher, if exist, the app returns the instance of the teacher'() {
        given:
            def teacherInstance = new Teacher(id: 1)
        and:
            mockDomain(Teacher, [teacherInstance])

        when:
            def model = controller.show(teacherInstance.id)
        then:
            model.teacherInstance.id == teacherInstance.id
    }

    def 'The method edit return an instance of an teacher if exists'(){
        given:
            def teacherInstance = new Teacher(id: 1)
        and:
            mockDomain(Teacher, [teacherInstance])

        when:
            def model = controller.edit(teacherInstance.id)
        then:
            model.teacherInstance.id == teacherInstance.id
    }

    def 'The method edit redirect to the list action if the teacher does not exists'(){
        given:
            mockDomain(Teacher)
        when:
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'The method update redirect to the list action if the teacher does not exists'(){
        given:
            mockDomain(Teacher)
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If somebody updates the teacher before, it renders again the original object'(){
        given:
            def teacherInstance = new Teacher()
        and:
            mockDomain(Teacher, [teacherInstance])
        and:
            teacherInstance.metaClass.getVersion { 2 }

        when:
            def model = controller.update(teacherInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.teacherInstance.id == teacherInstance.id
    }

    def 'If there is an error updating the teacher, the app renders again the edit view'(){
        given:
            def teacherInstance = new Teacher()
        and:
            mockDomain(Teacher, [teacherInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = null
            controller.params.password = "password"
            def model = controller.update(teacherInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.teacherInstance.id == teacherInstance.id
    }

    def 'If there are no error updating the teacher, the app shows the teacher and shows a message'(){
        given:
            def teacherInstance = new Teacher()
        and:
            mockDomain(Teacher, [teacherInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "username"
            controller.params.password = "password"
            def model = controller.update(teacherInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == teacherInstance.id
    }

    def 'The method delete redirect to the list action if the teacher does not exists'(){
        given:
            mockDomain(Teacher)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If deleting an exception is thrown, the app redirects to show the teacher and shows a message'(){
        given:
            def teacherInstance = new Teacher(id:1)
        and:
            mockDomain(Teacher, [teacherInstance])
            teacherInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }

        when:
            controller.delete(teacherInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
            redirectArgs.id == teacherInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def teacherInstance = new Teacher(id:1)
        and:
            mockDomain(Teacher, [teacherInstance])

        when:
            controller.delete(teacherInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }
}

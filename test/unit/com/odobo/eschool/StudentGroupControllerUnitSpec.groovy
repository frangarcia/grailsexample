package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException

class StudentGroupControllerUnitSpec extends ControllerSpec {

    def setup() {
        controller.metaClass.message { args -> 'message' }
    }

    def 'When visiting the index method, the application redirects to the list method'(){
        when:
            controller.index()
        then:
            redirectArgs.action == 'list'
    }

    def 'The method create return an instance of an studentGroup'(){
        given:
            controller.params.name = "Student Group 1"
        when:
            def model = controller.create()
        then:
            model.studentGroupInstance.name == "Student Group 1"
    }

    def 'If an error occurs when saving the studentGroup, the create view is rendered again'(){
        given:
            mockDomain(StudentGroup)

        when:
            controller.params.name = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new studentGroup'(){
        given:
            mockDomain(StudentGroup)
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])

        when:
            controller.params.name = "Student Group"
            controller.params.course = courseInstance
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an studentGroup, if does not exist, the app redirect to the list of studentGroup'() {
        given:
            mockDomain(StudentGroup)

        when:
            controller.params.id = 123
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an studentGroup, if exist, the app returns the instance of the studentGroup'() {
        given:
            def studentGroupInstance = new StudentGroup(id: 1)
        and:
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            def model = controller.show(studentGroupInstance.id)
        then:
            model.studentGroupInstance.id == studentGroupInstance.id
    }

    def 'The method edit return an instance of an studentGroup if exists'(){
        given:
            def studentGroupInstance = new StudentGroup(id: 1)
        and:
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            def model = controller.edit(studentGroupInstance.id)
        then:
            model.studentGroupInstance.id == studentGroupInstance.id
    }

    def 'The method edit redirect to the list action if the studentGroup does not exists'(){
        given:
            mockDomain(StudentGroup)
        when:
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'The method update redirect to the list action if the studentGroup does not exists'(){
        given:
            mockDomain(StudentGroup)
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If somebody updates the studentGroup before, it renders again the original object'(){
        given:
            def studentGroupInstance = new StudentGroup()
        and:
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            studentGroupInstance.metaClass.getVersion { 2 }

        when:
            def model = controller.update(studentGroupInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.studentGroupInstance.id == studentGroupInstance.id
    }

    def 'If there is an error updating the studentGroup, the app renders again the edit view'(){
        given:
            def studentGroupInstance = new StudentGroup()
        and:
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.params.name = "Fran"
            def model = controller.update(studentGroupInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.studentGroupInstance.id == studentGroupInstance.id
    }

    def 'If there are no error updating the studentGroup, the app shows the studentGroup and shows a message'(){
        given:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
            def studentGroupInstance = new StudentGroup(course:courseInstance)
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.params.name = "Student Group"
            controller.params.courseInstance = courseInstance
            def model = controller.update(studentGroupInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == studentGroupInstance.id
    }

    def 'The method delete redirect to the list action if the studentGroup does not exists'(){
        given:
            mockDomain(StudentGroup)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If deleting an exception is thrown, the app redirects to show the studentGroup and shows a message'(){
        given:
            def studentGroupInstance = new StudentGroup(id:1)
        and:
            mockDomain(StudentGroup, [studentGroupInstance])
            studentGroupInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }

        when:
            controller.delete(studentGroupInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
            redirectArgs.id == studentGroupInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def studentGroupInstance = new StudentGroup(id:1)
        and:
            mockDomain(StudentGroup, [studentGroupInstance])

        when:
            controller.delete(studentGroupInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }
}

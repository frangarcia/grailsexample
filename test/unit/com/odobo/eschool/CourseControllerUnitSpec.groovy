package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException
import grails.plugins.springsecurity.SpringSecurityService

class CourseControllerUnitSpec extends ControllerSpec {

    def setup() {
        controller.metaClass.message { args -> 'message' }
    }

    def 'When visiting the index method, the application redirects to the list method'(){
        when:
            controller.index()
        then:
            redirectArgs.action == 'list'
    }

    def 'The method create return an instance of an course'(){
        given:
            controller.params.name = "Course"
        when:
            def model = controller.create()
        then:
            model.courseInstance.name == "Course"
    }

    def 'If an error occurs when saving the course, the create view is rendered again'(){
        given:
            mockDomain(Course)

        when:
            controller.params.name = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new course'(){
        given:
            mockDomain(Course)
        and:
            def studyYearInstance = new StudyYear()
            mockDomain(StudyYear, [studyYearInstance])

        when:
            controller.params.name = "Course"
            controller.params.studyYear = studyYearInstance
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an course, if does not exist, the app redirect to the list of course'() {
        given:
            mockDomain(Course)

        when:
            controller.params.id = 123
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an course, if exist, the app returns the instance of the course'() {
        given:
            def courseInstance = new Course(id: 1)
        and:
            mockDomain(Course, [courseInstance])

        when:
            def model = controller.show(courseInstance.id)
        then:
            model.courseInstance.id == courseInstance.id
    }

    def 'The method edit return an instance of an course if exists'(){
        given:
            def courseInstance = new Course(id: 1)
        and:
            mockDomain(Course, [courseInstance])

        when:
            def model = controller.edit(courseInstance.id)
        then:
            model.courseInstance.id == courseInstance.id
    }

    def 'The method edit redirect to the list action if the course does not exists'(){
        given:
            mockDomain(Course)
        when:
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'The method update redirect to the list action if the course does not exists'(){
        given:
            mockDomain(Course)
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If somebody updates the course before, it renders again the original object'(){
        given:
            def courseInstance = new Course()
        and:
            mockDomain(Course, [courseInstance])
        and:
            courseInstance.metaClass.getVersion { 2 }

        when:
            def model = controller.update(courseInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.courseInstance.id == courseInstance.id
    }

    def 'If there is an error updating the course, the app renders again the edit view'(){
        given:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studyYearInstance = new StudyYear()
            mockDomain(StudyYear, [studyYearInstance])
        and:
            controller.params.name = null
            controller.params.studyYear = studyYearInstance

        when:
            controller.update(courseInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.courseInstance.id == courseInstance.id
    }

    def 'If there are no errors updating the course, the app shows the course and shows a message'(){
        given:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studyYearInstance = new StudyYear()
            mockDomain(StudyYear, [studyYearInstance])
        and:
            controller.params.name = "Course"
            controller.params.studyYear = studyYearInstance

        when:
            def model = controller.update(courseInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == courseInstance.id
    }

    def 'The method delete redirect to the list action if the course does not exists'(){
        given:
            mockDomain(Course)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If deleting an exception is thrown, the app redirects to show the course and shows a message'(){
        given:
            def courseInstance = new Course(id:1)
        and:
            mockDomain(Course, [courseInstance])
            courseInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }

        when:
            controller.delete(courseInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
            redirectArgs.id == courseInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def courseInstance = new Course(id:1)
        and:
            mockDomain(Course, [courseInstance])

        when:
            controller.delete(courseInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'Showing student groups returns a course instance'(){
        given:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])

        when:
            def model = controller.showStudentGroups(courseInstance.id)
        then:
            model.courseInstance == courseInstance
    }

    def 'Showing the course for a student'(){
        given:
            def courseInstance1 = new Course()
            def courseInstanceList = [courseInstance1]
            mockDomain(Course, courseInstanceList)
        and:
            def studentGroupInstance = new StudentGroup(course: courseInstance1)
            mockDomain(StudentGroup, [studentGroupInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])
            studentInstance.addToStudentGroups(studentGroupInstance)
        and:
            def springSecurityService = new SpringSecurityService()
            springSecurityService.metaClass.getCurrentUser { studentInstance }
            controller.springSecurityService = springSecurityService

        when:
            def model = controller.myCoursesAsStudent()
        then:
            model.courseInstanceList == courseInstanceList

    }

    def 'Showing the absences as student returns the course instance and and the absence list'(){
        given:
            def courseInstance = new Course()
            mockDomain(Course, [courseInstance])
        and:
            def studentInstance = new Student()
            mockDomain(Student, [studentInstance])
        and:
            def absence1 = new Absence(course: courseInstance, student: studentInstance)
            def absence2 = new Absence(course: courseInstance, student: studentInstance)
            def absenceInstanceList = [absence1, absence2]
            mockDomain(Absence, absenceInstanceList)
        and:
            def springSecurityService = new SpringSecurityService()
            springSecurityService.metaClass.getCurrentUser { studentInstance }
            controller.springSecurityService = springSecurityService

        when:
            def model = controller.absences(courseInstance.id)
        then:
            model.courseInstance == courseInstance
            model.absenceInstanceList == absenceInstanceList
    }

    def 'Showing the course for a teacher'(){
        given:
            def courseInstance1 = new Course()
            mockDomain(Course, [courseInstance1])
        and:
            def studentGroupInstance = new StudentGroup(course: courseInstance1)
            def studentGroupInstanceList = [studentGroupInstance]
            mockDomain(StudentGroup, studentGroupInstanceList)
        and:
            def teacherInstance = new Teacher()
            mockDomain(Teacher, [teacherInstance])
            teacherInstance.metaClass.getStudentGroups { studentGroupInstanceList }
        and:
            def springSecurityService = new SpringSecurityService()
            springSecurityService.metaClass.getCurrentUser { teacherInstance }
            controller.springSecurityService = springSecurityService

        when:
            def model = controller.myCoursesAsTeacher()
        then:
            model.studentGroupInstanceList == studentGroupInstanceList
            model.teacherInstance == teacherInstance
    }
}

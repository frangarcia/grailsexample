package com.odobo.eschool

import grails.plugin.spock.ControllerSpec
import org.springframework.dao.DataIntegrityViolationException

class StudentControllerUnitSpec extends ControllerSpec {

    def setup() {
        User.metaClass.encodePassword { "encoded" }
        User.metaClass.isDirty { false }

        controller.metaClass.message { args -> 'message' }
    }

    def 'When visiting the index method, the application redirects to the list method'(){
        when:
            controller.index()
        then:
            redirectArgs.action == 'list'
    }

    def 'The method create return an instance of an student'(){
        given:
            controller.params.username = "frangarcia"
        when:
            def model = controller.create()
        then:
            model.studentInstance.username == "frangarcia"
    }

    def 'If an error occurs when saving the student, the create view is rendered again'(){
        given:
            mockDomain(Student)

        when:
            controller.params.username = null
            controller.save()
        then:
            renderArgs.view == 'create'
    }

    def 'After saving, the application show the new student'(){
        given:
            mockDomain(Student)

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "frangarcia"
            controller.params.password = "password"
            controller.save()
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
    }

    def 'When showing an student, if does not exist, the app redirect to the list of student'() {
        given:
            mockDomain(Student)

        when:
            controller.params.id = 123
            controller.show()
        then:
            redirectArgs.action == "list"
    }

    def 'When showing an student, if exist, the app returns the instance of the student'() {
        given:
            def studentInstance = new Student(id: 1)
        and:
            mockDomain(Student, [studentInstance])

        when:
            def model = controller.show(studentInstance.id)
        then:
            model.studentInstance.id == studentInstance.id
    }

    def 'The method edit return an instance of an student if exists'(){
        given:
            def studentInstance = new Student(id: 1)
        and:
            mockDomain(Student, [studentInstance])

        when:
            def model = controller.edit(studentInstance.id)
        then:
            model.studentInstance.id == studentInstance.id
    }

    def 'The method edit redirect to the list action if the student does not exists'(){
        given:
            mockDomain(Student)
        when:
            def model = controller.edit(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'The method update redirect to the list action if the student does not exists'(){
        given:
            mockDomain(Student)
        when:
            def model = controller.update(12,0)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If somebody updates the student before, it renders again the original object'(){
        given:
            def studentInstance = new Student()
        and:
            mockDomain(Student, [studentInstance])
        and:
            studentInstance.metaClass.getVersion { 2 }

        when:
            def model = controller.update(studentInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.studentInstance.id == studentInstance.id
    }

    def 'If there is an error updating the student, the app renders again the edit view'(){
        given:
            def studentInstance = new Student()
        and:
            mockDomain(Student, [studentInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = null
            controller.params.password = "password"
            def model = controller.update(studentInstance.id, -1)
        then:
            renderArgs.view == "edit"
            renderArgs.model.studentInstance.id == studentInstance.id
    }

    def 'If there are no error updating the student, the app shows the student and shows a message'(){
        given:
            def studentInstance = new Student()
        and:
            mockDomain(Student, [studentInstance])

        when:
            controller.params.name = "Fran"
            controller.params.surnames = "Garcia"
            controller.params.username = "username"
            controller.params.password = "password"
            def model = controller.update(studentInstance.id, -1)
        then:
            controller.flash.message == "message"
            redirectArgs.action == 'show'
            redirectArgs.id == studentInstance.id
    }

    def 'The method delete redirect to the list action if the student does not exists'(){
        given:
            mockDomain(Student)
        when:
            def model = controller.delete(12)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }

    def 'If deleting an exception is thrown, the app redirects to show the student and shows a message'(){
        given:
            def studentInstance = new Student(id:1)
        and:
            mockDomain(Student, [studentInstance])
            studentInstance.metaClass.delete { Map params -> throw new DataIntegrityViolationException('message') }

        when:
            controller.delete(studentInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'show'
            redirectArgs.id == studentInstance.id
    }

    def 'If deleting is successful, the app redirects to list and shows a message'(){
        given:
            def studentInstance = new Student(id:1)
        and:
            mockDomain(Student, [studentInstance])

        when:
            controller.delete(studentInstance.id)
        then:
            controller.flash.message == 'message'
            redirectArgs.action == 'list'
    }
}

package com.odobo.eschool

import grails.plugin.spock.IntegrationSpec
import grails.plugins.springsecurity.SpringSecurityService

class CourseControllerIntegrationSpec extends IntegrationSpec {

    def courseController

    def setup() {
        courseController = new CourseController()

    }

    def 'Showing the grades as student returns the course instance and and the grade list by semester'(){
        given:
            def courseInstance = Course.build()
        and:
            def studentInstance = Student.build()
        and:
            def grade1 = Grade.build(course: courseInstance, student: studentInstance, grade: 5, semester: Semester.FIRST_SEMESTER)
            def grade2 = Grade.build(course: courseInstance, student: studentInstance, grade: 7, semester: Semester.FIRST_SEMESTER)
            def grade3 = Grade.build(course: courseInstance, student: studentInstance, grade: 7, semester: Semester.SECOND_SEMESTER)
            def grade4 = Grade.build(course: courseInstance, student: studentInstance, grade: 6, semester: Semester.SECOND_SEMESTER)
            def gradeFirstSemesterInstanceList = [grade1, grade2]
            def gradeSecondSemesterInstanceList = [grade3, grade4]

        and:
            def springSecurityService = new SpringSecurityService()
            springSecurityService.metaClass.getCurrentUser { studentInstance }
            courseController.springSecurityService = springSecurityService
        when:
            def model = courseController.grades(courseInstance.id)
        then:
            model.courseInstance == courseInstance
            model.gradeFirstSemesterInstanceList.containsAll(gradeFirstSemesterInstanceList)
            model.gradeSecondSemesterInstanceList.containsAll(gradeSecondSemesterInstanceList)
    }
}

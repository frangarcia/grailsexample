import grails.util.Environment
import com.odobo.eschool.Role
import com.odobo.eschool.User
import com.odobo.eschool.UserRole
import com.odobo.eschool.StudyYear
import com.odobo.eschool.Course
import com.odobo.eschool.StudentGroup
import com.odobo.eschool.Teacher
import com.odobo.eschool.Administrator
import com.odobo.eschool.Student
import com.odobo.eschool.Grade
import com.odobo.eschool.Semester
import com.odobo.eschool.StudentGroupTeacher
import java.text.SimpleDateFormat
import com.odobo.eschool.Absence

class BootStrap {

    def init = { servletContext ->
        switch (Environment.current.name){
            case "development": setUpDevelopmentEnvironment()
                break
            case "test":        setUpTestEnvironment()
                break
            case "production":  setUpProductionEnvironment()
                break
        }

    }
    def destroy = {
    }

    private void createUsersAndRoles(){
        def adminRole = new Role(authority: "ROLE_ADMIN").save()
        def adminUser = new Administrator(username:"admin", password:"admin", enabled:true, name:"Admin", surnames:"eSchool").save()
        new UserRole(user: adminUser, role: adminRole).save()

        def teacherRole = new Role(authority: "ROLE_TEACHER").save()
        def teacherMathsUser = new Teacher(username:"teacher1", password:"teacher1", enabled:true, name:"Teacher", surnames:"Maths1").save()
        new UserRole(user: teacherMathsUser, role: teacherRole).save()

        def teacherMathsUser2 = new Teacher(username:"teacher2", password:"teacher2", enabled:true, name:"Teacher", surnames:"Maths2").save()
        new UserRole(user: teacherMathsUser2, role: teacherRole).save()

        def studentRole = new Role(authority: "ROLE_STUDENT").save()
        def student1User = new Student(username:"student1", password:"student1", enabled:true, name:"Student", surnames:"1").save()
        new UserRole(user: student1User, role: studentRole).save()
        def student2User = new Student(username:"student2", password:"student2", enabled:true, name:"Student", surnames:"2").save()
        new UserRole(user: student2User, role: studentRole).save()

    }

    private void createCoursesWithStudentsAndGrades(){
        def studyYear2012 = new StudyYear(name:"2012").save()
        def studyYear2013 = new StudyYear(name:"2013").save()

        def courseMaths2013 = new Course(name: "Maths", studyYear: studyYear2013).save()
        def courseScience2013 = new Course(name: "Science", studyYear: studyYear2013).save()

        def studentGroupAMaths2013 = new StudentGroup(name: "A", course: courseMaths2013).save()
        def studentGroupBMaths2013 = new StudentGroup(name: "B", course: courseMaths2013).save()

        def studentGroupAScience2013 = new StudentGroup(name: "A", course: courseScience2013).save()
        def studentGroupBScience2013 = new StudentGroup(name: "B", course: courseScience2013).save()

        def teacherMaths1 = Teacher.findByUsername("teacher1")
        new StudentGroupTeacher(teacher: teacherMaths1, studentGroup: studentGroupAMaths2013, tutor: false).save()
        new StudentGroupTeacher(teacher: teacherMaths1, studentGroup: studentGroupBMaths2013, tutor: true).save()

        def student1 = Student.findByUsername("student1")
        student1.addToStudentGroups(studentGroupAMaths2013)
        student1.addToStudentGroups(studentGroupAScience2013)
        student1.save()

        def student2 = Student.findByUsername("student2")
        student2.addToStudentGroups(studentGroupBMaths2013)
        student2.addToStudentGroups(studentGroupBScience2013)
        student2.save()


        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('02/01/2013'), course: courseMaths2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('14/02/2013'), course: courseMaths2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('26/03/2013'), course: courseMaths2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('08/05/2013'), course: courseMaths2013, student: student1, justifiableCause: true).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('10/05/2013'), course: courseMaths2013, student: student1, justifiableCause: true).save()

        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('01/01/2013'), course: courseScience2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('13/02/2013'), course: courseScience2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('25/03/2013'), course: courseScience2013, student: student1, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('07/05/2013'), course: courseScience2013, student: student1, justifiableCause: true).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('19/05/2013'), course: courseScience2013, student: student1, justifiableCause: true).save()

        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('03/01/2013'), course: courseMaths2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('14/02/2013'), course: courseMaths2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('27/03/2013'), course: courseMaths2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('08/05/2013'), course: courseMaths2013, student: student2, justifiableCause: true).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('11/05/2013'), course: courseMaths2013, student: student2, justifiableCause: true).save()

        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('02/01/2013'), course: courseScience2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('14/02/2013'), course: courseScience2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('26/03/2013'), course: courseScience2013, student: student2, justifiableCause: false).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('08/05/2013'), course: courseScience2013, student: student2, justifiableCause: true).save()
        new Absence(date: new SimpleDateFormat('dd/MM/yyyy', Locale.ENGLISH).parse('20/05/2013'), course: courseScience2013, student: student2, justifiableCause: true).save()


        new Grade(grade:4, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:4.6, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:5, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:5.7, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:6.3, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:6.6, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:7.8, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:6.5, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:9.8, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:7.7, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:8.9, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()
        new Grade(grade:8, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student1).save()

        new Grade(grade:3, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:3.6, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:4, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:5.7, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:6.3, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:7.6, semester:Semester.FIRST_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:6.8, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:7.5, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:8.8, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:7.7, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:7.9, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()
        new Grade(grade:7, semester:Semester.SECOND_SEMESTER, course:courseMaths2013, student:student2).save()

        new Grade(grade:6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:6.6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:7, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:7.7, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:8.3, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:8.6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:9.8, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:8.5, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:9.8, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:9.7, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:8.9, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()
        new Grade(grade:9, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student1).save()

        new Grade(grade:5, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:5.6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:7.7, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:8.3, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:9.6, semester:Semester.FIRST_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:7.8, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:7.5, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:8.8, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:9.7, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:6.9, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        new Grade(grade:9, semester:Semester.SECOND_SEMESTER, course:courseScience2013, student:student2).save()
        
    }

    private void setUpDevelopmentEnvironment() {
        createUsersAndRoles()
        createCoursesWithStudentsAndGrades()
    }

    private void setUpTestEnvironment() {
    }

    private void setUpProductionEnvironment() {
    }
}

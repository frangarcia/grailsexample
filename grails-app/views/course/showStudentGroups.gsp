
<%@ page import="com.odobo.eschool.Course" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-course" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-course" class="content scaffold-list" role="main">
			<h1><g:message code="default.showStudentGroups.label" default="Show Student Groups" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
            <div>
                <g:each in="${courseInstance.studentGroups}" var="studentGroup">
                    <h1>${studentGroup}</h1>
                    <table>
                        <thead>
                        <tr>

                            <th><g:message code="userProfile.name.label" default="Name" /></th>

                            <th><g:message code="userProfile.surnames.label" default="Surnames" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${studentGroup.teachers}" status="i" var="teacherInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td><g:link controller="teacher" action="show" id="${teacherInstance.id}">${fieldValue(bean: teacherInstance, field: "name")}</g:link></td>

                                <td><g:link controller="teacher" action="show" id="${teacherInstance.id}">${fieldValue(bean: teacherInstance, field: "surnames")}</g:link></td>

                            </tr>
                        </g:each>
                        <g:each in="${studentGroup.students}" status="i" var="studentInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td><g:link controller="student" action="show" id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "name")}</g:link></td>

                                <td><g:link controller="student" action="show" id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "surnames")}</g:link></td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:each>
            </div>
		</div>
	</body>
</html>


<%@ page import="com.odobo.eschool.Course" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-course" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-course" class="content scaffold-list" role="main">
			<h1><g:message code="default.myCoursesAsStudent.label" default="My Courses as Student" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'course.name.label', default: 'Name')}" />
					
						<th><g:message code="course.studyYear.label" default="Study Year" /></th>

                        <th>Actions</th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${courseInstanceList}" status="i" var="courseInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: courseInstance, field: "studyYear")}</td>

                        <td>
                            <g:link action="absences" id="${courseInstance.id}">Show absences</g:link> |
                            <g:link action="grades" id="${courseInstance.id}">Show grades</g:link>
                        </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
	</body>
</html>

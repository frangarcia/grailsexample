<%@ page import="com.odobo.eschool.Course" %>



<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="course.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${courseInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'grades', 'error')} ">
	<label for="grades">
		<g:message code="course.grades.label" default="Grades" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${courseInstance?.grades?}" var="g">
    <li><g:link controller="grade" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="grade" action="create" params="['course.id': courseInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'grade.label', default: 'Grade')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'studentGroups', 'error')} ">
	<label for="studentGroups">
		<g:message code="course.studentGroups.label" default="Student Groups" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${courseInstance?.studentGroups?}" var="s">
    <li><g:link controller="studentGroup" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="studentGroup" action="create" params="['course.id': courseInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'studentGroup.label', default: 'StudentGroup')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: courseInstance, field: 'studyYear', 'error')} required">
	<label for="studyYear">
		<g:message code="course.studyYear.label" default="Study Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="studyYear" name="studyYear.id" from="${com.odobo.eschool.StudyYear.list()}" optionKey="id" required="" value="${courseInstance?.studyYear?.id}" class="many-to-one"/>
</div>



<%@ page import="com.odobo.eschool.Course" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <gvisualization:apiImport/>
	</head>
	<body>
		<a href="#list-course" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="myCoursesAsStudent"><g:message code="default.myCoursesAsStudent.label" default="My Courses as Student" /></g:link></li>
			</ul>
		</div>
		<div id="list-course" class="content scaffold-list" role="main">
			<h1><g:message code="default.grades.label" default="Grades in {0}" args="[courseInstance]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
            <g:if test="${gradeFirstSemesterInstanceList.size()>0}">
                <table>
                    <thead>
                    <tr>

                        <th><g:message code="course.grade.label" default="Grade" /></th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${gradeFirstSemesterInstanceList}" status="i" var="gradeInstance">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                            <td>${gradeInstance.grade}</td>

                        </tr>
                    </g:each>
                    <tr class="even">

                        <td><b>Average First Semester: ${g.formatNumber(number:averageGradeFirstSemesterByCourseAndStudent, maxFractionDigits: 2)}</b></td>

                    </tr>
                    </tbody>
                </table>
            </g:if>
            <g:if test="${gradeSecondSemesterInstanceList.size()>0}">
                <table>
                    <thead>
                    <tr>

                        <th><g:message code="course.grade.label" default="Grade" /></th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${gradeSecondSemesterInstanceList}" status="i" var="gradeInstance">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                            <td>${gradeInstance.grade}</td>

                        </tr>
                    </g:each>
                    <tr class="even">

                        <td><b>Average Second Semester: ${g.formatNumber(number:averageGradeSecondSemesterByCourseAndStudent, maxFractionDigits: 2)}</b></td>

                    </tr>
                    </tbody>
                </table>
            </g:if>
            <table>
                <thead>
                    <tr class="even">

                        <th>AVERAGE: ${g.formatNumber(number:averageGradeByCourseAndStudent, maxFractionDigits: 2)}</th>

                    </tr>
                </thead>
            </table>
            <div>
                <gvisualization:lineCoreChart elementId="linechart" title="My progression" width="850" height="600"
                                             columns="[['string','Semester'], ['number','Grade']]"
                                             data="${dataChart}" />
                <div id="linechart"></div>
            </div>
		</div>
	</body>
</html>

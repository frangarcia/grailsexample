
<%@ page import="com.odobo.eschool.Course" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-course" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="myCoursesAsStudent"><g:message code="default.myCoursesAsStudent.label" default="My Courses as Student" /></g:link></li>
			</ul>
		</div>
		<div id="list-course" class="content scaffold-list" role="main">
			<h1><g:message code="default.absences.label" default="Absences in {0}" args="[courseInstance]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="course.date.label" default="Date" /></th>
					
						<th><g:message code="course.justifiableCause.label" default="Justifiable Cause" /></th>


					</tr>
				</thead>
				<tbody>
				<g:each in="${absenceInstanceList}" status="i" var="absenceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					

						<td><g:formatDate date="${absenceInstance.date}" format="MM/dd/yyyy"/></td>

                        <td><g:if test="${absenceInstance.justifiableCause}">Yes</g:if><g:else>No</g:else></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
	</body>
</html>

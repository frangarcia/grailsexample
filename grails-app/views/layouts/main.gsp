<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>eSchool @ ODOBO - <g:layoutTitle default="eSchool @ ODOBO"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
        <g:javascript library="jquery" />
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<div id="grailsLogo" role="banner"><a href="http://www.odobo.com"><img src="${resource(dir: 'img', file: 'odobo-logo@2x.png')}" alt="ODOBO"/></a></div>
		<div id="topmenu">
            <sec:access expression="hasRole('ROLE_ADMIN')">
                <ul>
                    <li><g:link controller="administrator">Administrators</g:link></li>
                    <li><g:link controller="teacher">Teachers</g:link></li>
                    <li><g:link controller="student">Students</g:link></li>
                    <li><g:link controller="studyYear">Study Years</g:link></li>
                    <li><g:link controller="course">Courses</g:link></li>
                    <li><g:link controller="studentGroup">Groups</g:link></li>
                    <li><g:link controller="logout">Logout as <sec:username/></g:link></li>
                </ul>
            </sec:access>
            <sec:access expression="hasRole('ROLE_TEACHER')">
                <ul>
                    <li><g:link controller="course" action="myCoursesAsTeacher">My courses</g:link></li>
                    <li><g:link controller="logout">Logout as <sec:username/></g:link></li>
                </ul>
            </sec:access>
            <sec:access expression="hasRole('ROLE_STUDENT')">
                <ul>
                    <li><g:link controller="course" action="myCoursesAsStudent">My courses</g:link></li>
                    <li><g:link controller="logout">Logout as <sec:username/></g:link></li>
                </ul>
            </sec:access>
		</div>
        <g:layoutBody/>
		<div class="footer" role="contentinfo">
            Application developed by <a href="http://www.frangarcia.net">Francisco José García Rico</a> for <a href="http://www.odobo.com">ODOBO</a>
		</div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>

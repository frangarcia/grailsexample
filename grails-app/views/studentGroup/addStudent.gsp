
<%@ page import="com.odobo.eschool.StudentGroup" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'studentGroup.label', default: 'StudentGroup')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-studentGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="list-studentGroup" class="content scaffold-list" role="main">
    <h1><g:message code="default.addStudent.label" default="Add Student to ${studentGroupInstance}" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="username" title="${message(code: 'student.username.label', default: 'Username')}" />

            <g:sortableColumn property="name" title="${message(code: 'student.name.label', default: 'Name')}" />

            <g:sortableColumn property="surnames" title="${message(code: 'student.surnames.label', default: 'Surnames')}" />

            <th>Actions</th>


        </tr>
        </thead>
        <tbody>
        <g:each in="${studentInstanceList}" status="i" var="studentInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link controller="teacher" action="show" id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "username")}</g:link></td>

                <td>${fieldValue(bean: studentInstance, field: "name")}</td>

                <td>${fieldValue(bean: studentInstance, field: "surnames")}</td>

                <td>
                    <g:if test="${!studentInstance.belongsToStudentGroup(studentGroupInstance) }">
                        <g:link action="assignStudentToStudentGroup" id="${studentInstance.id}" params="[studentGroupInstanceId:studentGroupInstance.id]">Add to ${studentGroupInstance}</g:link>
                    </g:if>
                    <g:else>
                        Already in this group
                    </g:else>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>

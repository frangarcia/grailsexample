<%@ page import="com.odobo.eschool.StudentGroup" %>



<div class="fieldcontain ${hasErrors(bean: studentGroupInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="studentGroup.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${studentGroupInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: studentGroupInstance, field: 'course', 'error')} required">
	<label for="course">
		<g:message code="studentGroup.course.label" default="Course" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="course" name="course.id" from="${com.odobo.eschool.Course.list()}" optionKey="id" required="" value="${studentGroupInstance?.course?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: studentGroupInstance, field: 'students', 'error')} ">
	<label for="students">
		<g:message code="studentGroup.students.label" default="Students" />
		
	</label>
	<g:select name="students" from="${com.odobo.eschool.Student.list()}" multiple="multiple" optionKey="id" size="5" value="${studentGroupInstance?.students*.id}" class="many-to-many"/>
</div>


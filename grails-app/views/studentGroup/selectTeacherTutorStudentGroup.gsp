<%@ page import="com.odobo.eschool.StudentGroup" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'studentGroup.label', default: 'StudentGroup')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-studentGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-studentGroup" class="content scaffold-create" role="main">
			<h1><g:message code="default.assignStudentGroupToTeacher.label" default="Assign Student Group to Teacher" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${studentGroupInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${studentGroupInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="assignTeacherToStudentGroup" >
				<fieldset class="form">
                    <div class="fieldcontain required">
                        <label for="course">
                            <g:message code="studentGroup.course.label" default="Course" />
                        </label>
                        ${studentGroupInstance?.course}
                    </div>

                    <div class="fieldcontain required">
                        <label for="group">
                            <g:message code="studentGroup.name.label" default="Group" />
                        </label>
                        ${studentGroupInstance?.name}
                    </div>

                    <div class="fieldcontain required">
                        <label for="teacher">
                            <g:message code="studentGroup.teacher.label" default="Teacher" />
                        </label>
                        ${teacherInstance}
                    </div>

                    <div class="fieldcontain ${hasErrors(bean: studentGroupInstance, field: 'students', 'error')} ">
                        <label for="isTutor">
                            <g:message code="studentGroup.isTutor.label" default="Is this teacher a tutor of this group?" />

                        </label>
                        <g:checkBox name="isTutor"/>
                    </div>
                    <g:hiddenField name="teacherInstance.id" value="${teacherInstance.id}"/>
                    <g:hiddenField name="studentGroupInstance.id" value="${studentGroupInstance.id}"/>
                </fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.assignGroup.label', default: 'Assign group')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>

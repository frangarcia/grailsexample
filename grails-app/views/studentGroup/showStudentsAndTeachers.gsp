
<%@ page import="com.odobo.eschool.StudentGroup" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'studentGroup.label', default: 'StudentGroup')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-studentGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="addTeacher" id="${studentGroupInstance.id}"><g:message code="default.addTeacher.label" default="Add teacher" /></g:link></li>
                <li><g:link class="create" action="addStudent" id="${studentGroupInstance.id}"><g:message code="default.addStudent.label" default="Add student" /></g:link></li>
            </ul>
		</div>
		<div id="list-studentGroup" class="content scaffold-list" role="main">
			<h1><g:message code="default.showStudentsAndTeachers.label" default="Show Students & Teachers for group ${studentGroupInstance}" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>

						<th><g:message code="userProfile.name.label" default="Name" /></th>

                        <th><g:message code="userProfile.surnames.label" default="Surnames" /></th>

                        <th><g:message code="userProfile.role.label" default="Role" /></th>

                    </tr>
				</thead>
				<tbody>
				<g:each in="${teacherInstanceList}" status="i" var="teacherInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${teacherInstance.id}">${fieldValue(bean: teacherInstance, field: "teacher.name")}</g:link></td>

                        <td><g:link action="show" id="${teacherInstance.id}">${fieldValue(bean: teacherInstance, field: "teacher.surnames")}</g:link></td>

                        <td>Teacher ${teacherInstance.tutor ? 'Tutor' : ''}</td>
					
					</tr>
				</g:each>
                <g:each in="${studentInstanceList}" status="i" var="studentInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                        <td><g:link action="show" id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "name")}</g:link></td>

                        <td><g:link action="show" id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "surnames")}</g:link></td>

                        <td>Student</td>

                    </tr>
                </g:each>
				</tbody>
			</table>
		</div>
	</body>
</html>

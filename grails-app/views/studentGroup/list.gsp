
<%@ page import="com.odobo.eschool.StudentGroup" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'studentGroup.label', default: 'StudentGroup')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-studentGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-studentGroup" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>

						<th><g:message code="studentGroup.course.label" default="Course" /></th>

                        <g:sortableColumn property="name" title="${message(code: 'studentGroup.name.label', default: 'Name')}" />

                        <th><g:message code="studentGroup.studentsAndTeachers.label" default="Students & Teachers" /></th>


                    </tr>
				</thead>
				<tbody>
				<g:each in="${studentGroupInstanceList}" status="i" var="studentGroupInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: studentGroupInstance, field: "course")}</td>

                        <td><g:link action="show" id="${studentGroupInstance.id}">${fieldValue(bean: studentGroupInstance, field: "name")}</g:link></td>

                        <td><g:link action="showStudentsAndTeachers" id="${studentGroupInstance.id}">Show</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${studentGroupInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

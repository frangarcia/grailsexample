
<%@ page import="com.odobo.eschool.Grade" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'grade.label', default: 'Grade')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-grade" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="showGradesByStudentAndStudentGroup" id="${studentGroupInstance.id}"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create" id="${studentGroupInstance.id}"><g:message code="default.create.label" args="[entityName]" /></g:link></li>
            </ul>
		</div>
		<div id="show-grade" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list grade">
			
				<g:if test="${gradeInstance?.grade}">
				<li class="fieldcontain">
					<span id="grade-label" class="property-label"><g:message code="grade.grade.label" default="Grade" /></span>
					
						<span class="property-value" aria-labelledby="grade-label"><g:fieldValue bean="${gradeInstance}" field="grade"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gradeInstance?.course}">
				<li class="fieldcontain">
					<span id="course-label" class="property-label"><g:message code="grade.course.label" default="Course" /></span>
					
						<span class="property-value" aria-labelledby="course-label">${gradeInstance?.course?.encodeAsHTML()}</span>
					
				</li>
				</g:if>
			
				<g:if test="${gradeInstance?.semester}">
				<li class="fieldcontain">
					<span id="semester-label" class="property-label"><g:message code="grade.semester.label" default="Semester" /></span>
					
						<span class="property-value" aria-labelledby="semester-label"><g:fieldValue bean="${gradeInstance}" field="semester"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${gradeInstance?.student}">
				<li class="fieldcontain">
					<span id="student-label" class="property-label"><g:message code="grade.student.label" default="Student" /></span>
					
						<span class="property-value" aria-labelledby="student-label">${gradeInstance?.student?.encodeAsHTML()}</span>
					
				</li>
				</g:if>
			
			</ol>
		</div>
	</body>
</html>

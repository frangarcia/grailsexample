
<%@ page import="com.odobo.eschool.Grade" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'grade.label', default: 'Grade')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-grade" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create" id="${studentGroupInstance.id}"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
		</div>
		<div id="list-grade" class="content scaffold-list" role="main">
			<h1><g:message code="default.listGradesByCourse.label" args="[studentGroupInstance]" default="List grades in {0}" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
            <div id="gradeList">
                <g:each in="${studentGroupInstance.students}" var="studentInstance">
                    <h1>${studentInstance}</h1>
                    <table>
                        <thead>
                            <tr>

                                <g:sortableColumn property="grade" title="${message(code: 'grade.grade.label', default: 'Grade')}" />

                                <g:sortableColumn property="semester" title="${message(code: 'grade.semester.label', default: 'Semester')}" />

                                <th>Actions</th>

                            </tr>
                        </thead>
                        <tbody>
                        <g:each in="${studentInstance.getGradesByCourse(studentGroupInstance.course)}" status="i" var="gradeInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td>${fieldValue(bean: gradeInstance, field: "grade")}</td>

                                <td>${fieldValue(bean: gradeInstance, field: "semester")}</td>

                                <td>
                                    <g:if test="${amITutor}">
                                        <g:link action="edit"
                                                id="${gradeInstance.id}"
                                                params="[studentGroupId:studentGroupInstance.id]">Edit</g:link>
                                        |
                                        <g:remoteLink action="delete"
                                                        id="${gradeInstance.id}"
                                                        update="gradeList"
                                                        params="[studentGroupId:studentGroupInstance.id]">Delete</g:remoteLink>
                                    </g:if>

                                </td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:each>
            </div>
		</div>
	</body>
</html>

<g:each in="${studentGroupInstance.students}" var="studentInstance">
    <h1>${studentInstance}</h1>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="grade" title="${message(code: 'grade.grade.label', default: 'Grade')}" />

            <g:sortableColumn property="semester" title="${message(code: 'grade.semester.label', default: 'Semester')}" />

            <th>Actions</th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${studentInstance.getGradesByCourse(studentGroupInstance.course)}" status="i" var="gradeInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>${fieldValue(bean: gradeInstance, field: "grade")}</td>

                <td>${fieldValue(bean: gradeInstance, field: "semester")}</td>

                <td>
                    <g:link action="edit"
                            id="${gradeInstance.id}"
                            params="[studentGroupId:studentGroupInstance.id]">Edit</g:link>
                    |
                    <g:remoteLink action="delete"
                                  id="${gradeInstance.id}"
                                  update="gradeList"
                                  params="[studentGroupId:studentGroupInstance.id]">Delete</g:remoteLink>

                </td>

            </tr>
        </g:each>
        </tbody>
    </table>
</g:each>
<%@ page import="com.odobo.eschool.Grade" %>



<div class="fieldcontain ${hasErrors(bean: gradeInstance, field: 'grade', 'error')} required">
	<label for="grade">
		<g:message code="grade.grade.label" default="Grade" />
		<span class="required-indicator">*</span>
	</label>
    <g:textField name="grade" required="" value="${gradeInstance?.grade}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: gradeInstance, field: 'semester', 'error')} required">
	<label for="semester">
		<g:message code="grade.semester.label" default="Semester" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="semester" from="${com.odobo.eschool.Semester?.values()}" keys="${com.odobo.eschool.Semester.values()*.name()}" required="" value="${gradeInstance?.semester?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: gradeInstance, field: 'student', 'error')} required">
	<label for="student">
		<g:message code="grade.student.label" default="Student" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="student" name="student.id" from="${com.odobo.eschool.StudentGroup.get(studentGroupInstance.id).students}" optionKey="id" required="" value="${gradeInstance?.student?.id}" class="many-to-one"/>
</div>

<g:hiddenField name="studentGroupInstance.id" value="${studentGroupInstance.id}"/>
<g:hiddenField name="course.id" value="${studentGroupInstance.course.id}"/>


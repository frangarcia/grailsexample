<%@ page import="com.odobo.eschool.Administrator" %>



<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="administrator.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${administratorInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="administrator.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:passwordField name="password" required="" value=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="administrator.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${administratorInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'surnames', 'error')} required">
	<label for="surnames">
		<g:message code="administrator.surnames.label" default="Surnames" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="surnames" required="" value="${administratorInstance?.surnames}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="administrator.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${administratorInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="administrator.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${administratorInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="administrator.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${administratorInstance?.enabled}" />
</div>

<div class="fieldcontain ${hasErrors(bean: administratorInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="administrator.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${administratorInstance?.passwordExpired}" />
</div>


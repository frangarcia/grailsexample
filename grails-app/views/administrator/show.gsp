
<%@ page import="com.odobo.eschool.Administrator" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'administrator.label', default: 'Administrator')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-administrator" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-administrator" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list administrator">
			
				<g:if test="${administratorInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="administrator.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${administratorInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="administrator.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${administratorInstance}" field="password"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="administrator.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${administratorInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.surnames}">
				<li class="fieldcontain">
					<span id="surnames-label" class="property-label"><g:message code="administrator.surnames.label" default="Surnames" /></span>
					
						<span class="property-value" aria-labelledby="surnames-label"><g:fieldValue bean="${administratorInstance}" field="surnames"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.accountExpired}">
				<li class="fieldcontain">
					<span id="accountExpired-label" class="property-label"><g:message code="administrator.accountExpired.label" default="Account Expired" /></span>
					
						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${administratorInstance?.accountExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.accountLocked}">
				<li class="fieldcontain">
					<span id="accountLocked-label" class="property-label"><g:message code="administrator.accountLocked.label" default="Account Locked" /></span>
					
						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${administratorInstance?.accountLocked}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.enabled}">
				<li class="fieldcontain">
					<span id="enabled-label" class="property-label"><g:message code="administrator.enabled.label" default="Enabled" /></span>
					
						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${administratorInstance?.enabled}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${administratorInstance?.passwordExpired}">
				<li class="fieldcontain">
					<span id="passwordExpired-label" class="property-label"><g:message code="administrator.passwordExpired.label" default="Password Expired" /></span>
					
						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${administratorInstance?.passwordExpired}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${administratorInstance?.id}" />
					<g:link class="edit" action="edit" id="${administratorInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>

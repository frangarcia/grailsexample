
<%@ page import="com.odobo.eschool.Administrator" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'administrator.label', default: 'Administrator')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-administrator" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-administrator" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="username" title="${message(code: 'administrator.username.label', default: 'Username')}" />

						<g:sortableColumn property="name" title="${message(code: 'administrator.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="surnames" title="${message(code: 'administrator.surnames.label', default: 'Surnames')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${administratorInstanceList}" status="i" var="administratorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${administratorInstance.id}">${fieldValue(bean: administratorInstance, field: "username")}</g:link></td>

						<td>${fieldValue(bean: administratorInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: administratorInstance, field: "surnames")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${administratorInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

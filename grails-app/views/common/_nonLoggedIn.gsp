<h1>Welcome to eSchool @ ODOBO</h1>
<p>You are welcome to eSchool @ ODOBO, the online web application to manage all the information at your high school or university. This application has 3 different roles (admin, teacher and student)
and there are some users created with all of these roles so you can interact with the application. </p>

<div id="controller-list" role="navigation">
    <h2>These are the users with their usernames and passwords:</h2>
    <ul>
        <li>Administrator user (username:'admin', password:'admin')</li>
        <li>Teacher user (username:'teacher1', password:'teacher1')</li>
        <li>Teacher user (username:'teacher2', password:'teacher2')</li>
        <li>Student user (username:'student1', password:'student1')</li>
        <li>Student user (username:'student2', password:'student2')</li>
    </ul>
</div>
<p>
    With these data, the first thing you should <g:link controller="login" action="auth">go to the login page</g:link>  and try the application with different users.
</p>

<p>
    According to the challenge, I have developed the next features in the application:
</p>
<ul>
    <li>Security with Spring Security Core plugin. 3 different roles (admin, teacher and student) for each actor in the application. The user must be logged in to play with the application</li>
    <li>The admin users can manage administrators, teachers, students, student groups, courses and study years. This has been developed mostly using scaffolding</li>
    <li>All the teachers are able to manage the information about the absences but only the tutors can manage the grade information.</li>
    <li>A search feature has been developed when showing grades for a student group</li>
    <li>Students can basically show theis absences and grades. When viewing grades, a chart is also displayed.</li>
    <li>All the controllers has been tested with unit tests using the Spock framework (which has been installed as plugin)</li>
    <li>There is also an integration tests for something that was difficult to mock in a unit test</li>
</ul>

<p>
    If you have any doubt, do not hesitate to contact me using my email <a href="mailto:fgarciarico@gmail.com">fgarciarico@gmail.com</a> or my phone number (+34 677 819 474)
</p>
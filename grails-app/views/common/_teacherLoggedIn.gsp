<h1>You are logged in the application as <sec:username/></h1>
<p>With your roles you will be able to do the next tasks in the application:</p>

<div id="controller-list" role="navigation">
    <ul>
        <li><b>Manage your courses.</b> You will be able to view the grades and absences where you teach but also you can create/update/delete records in case you are the tutor of a student group</li>
    </ul>
</div>
<h1>You are logged in the application as <sec:username/></h1>
<p>With your roles you will be able to do the next tasks in the application:</p>

<div id="controller-list" role="navigation">
    <ul>
        <li><b>Manage users in the application.</b> You grants allows you to create administrators, teachers and students</li>
        <li><b>Manage study years.</b> Some of them are already created, but every year you would need to create a new one in order to create the courses properly</li>
        <li><b>Manage courses.</b> Create as many courses as you want for each study year</li>
        <li><b>Manage student groups.</b> Create, edit and delete groups and assign student to them</li>
    </ul>
</div>
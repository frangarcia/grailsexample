<h1>You are logged in the application as <sec:username/></h1>
<p>With your roles you will be able to do the next tasks in the application:</p>

<div id="controller-list" role="navigation">
    <ul>
        <li><b>View your absences.</b> You will be able to view how many absences you have for each of your courses</li>
        <li><b>View your grades.</b> You will have access to your grades and the averages</li>
    </ul>
</div>
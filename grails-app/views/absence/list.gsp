
<%@ page import="com.odobo.eschool.Absence" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absence.label', default: 'Absence')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-absence" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-absence" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="date" title="${message(code: 'absence.date.label', default: 'Date')}" />
					
						<th><g:message code="absence.course.label" default="Course" /></th>
					
						<g:sortableColumn property="justifiableCause" title="${message(code: 'absence.justifiableCause.label', default: 'Justifiable Cause')}" />
					
						<th><g:message code="absence.student.label" default="Student" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${absenceInstanceList}" status="i" var="absenceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${absenceInstance.id}">${fieldValue(bean: absenceInstance, field: "date")}</g:link></td>
					
						<td>${fieldValue(bean: absenceInstance, field: "course")}</td>
					
						<td><g:formatBoolean boolean="${absenceInstance.justifiableCause}" /></td>
					
						<td>${fieldValue(bean: absenceInstance, field: "student")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${absenceInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

<%@ page import="com.odobo.eschool.Absence" %>



<div class="fieldcontain ${hasErrors(bean: absenceInstance, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="absence.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="day"  value="${absenceInstance?.date}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: absenceInstance, field: 'justifiableCause', 'error')} ">
	<label for="justifiableCause">
		<g:message code="absence.justifiableCause.label" default="Justifiable Cause" />
		
	</label>
	<g:checkBox name="justifiableCause" value="${absenceInstance?.justifiableCause}" />
</div>

<div class="fieldcontain ${hasErrors(bean: absenceInstance, field: 'student', 'error')} required">
	<label for="student">
		<g:message code="absence.student.label" default="Student" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="student" name="student.id" from="${com.odobo.eschool.StudentGroup.get(studentGroupInstance.id).students}" optionKey="id" required="" value="${absenceInstance?.student?.id}" class="many-to-one"/>
</div>
<g:hiddenField name="studentGroupInstance.id" value="${studentGroupInstance.id}"/>
<g:hiddenField name="course.id" value="${studentGroupInstance.course.id}"/>


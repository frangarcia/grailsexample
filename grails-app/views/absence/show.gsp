
<%@ page import="com.odobo.eschool.Absence" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absence.label', default: 'Absence')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-absence" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="showAbsencesByStudentAndStudentGroup" id="${studentGroupInstance.id}"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
		</div>
		<div id="show-absence" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list absence">
			
				<g:if test="${absenceInstance?.date}">
				<li class="fieldcontain">
					<span id="date-label" class="property-label"><g:message code="absence.date.label" default="Date" /></span>
					
						<span class="property-value" aria-labelledby="date-label"><g:formatDate date="${absenceInstance?.date}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${absenceInstance?.course}">
				<li class="fieldcontain">
					<span id="course-label" class="property-label"><g:message code="absence.course.label" default="Course" /></span>
					
						<span class="property-value" aria-labelledby="course-label">${absenceInstance?.course?.encodeAsHTML()}</span>
					
				</li>
				</g:if>
			
				<li class="fieldcontain">
					<span id="justifiableCause-label" class="property-label"><g:message code="absence.justifiableCause.label" default="Justifiable Cause" /></span>
					
						<span class="property-value" aria-labelledby="justifiableCause-label"><g:formatBoolean boolean="${absenceInstance?.justifiableCause}" /></span>
					
				</li>

				<g:if test="${absenceInstance?.student}">
				<li class="fieldcontain">
					<span id="student-label" class="property-label"><g:message code="absence.student.label" default="Student" /></span>
					
						<span class="property-value" aria-labelledby="student-label">${absenceInstance?.student?.encodeAsHTML()}</span>
					
				</li>
				</g:if>
			
			</ol>
		</div>
	</body>
</html>

<%@ page import="com.odobo.eschool.Absence" %>

<g:each in="${studentGroupInstance.students}" var="studentInstance">
    <h1>${studentInstance}</h1>
    <table>
        <thead>
        <tr>

            <th>${message(code: 'absence.date.label', default: 'Date')}</th>

            <th>${message(code: 'absence.justifiableCause.label', default: 'Justifiable Cause')}</th>

            <th>Actions</th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${studentInstance.getAbsencesByCourse(studentGroupInstance.course)}" status="i" var="absenceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:formatDate date="${absenceInstance.date}" format="MM/dd/yyyy"/></td>

                <td>${absenceInstance.justifiableCause ? 'Yes': 'No'}</td>

                <td>
                    <g:remoteLink action="changeJustifiable"
                                  id="${absenceInstance.id}"
                                  update="absenceList"
                                  params="[studentGroupId:studentGroupInstance.id]">${absenceInstance.justifiableCause ? 'Mark as unjustifiable' : 'Mark as justifiable' }</g:remoteLink>
                    | <g:remoteLink action="delete"
                                    id="${absenceInstance.id}"
                                    update="absenceList"
                                    params="[studentGroupId:studentGroupInstance.id]">Delete</g:remoteLink>
                </td>

            </tr>
        </g:each>
        </tbody>
    </table>
</g:each>

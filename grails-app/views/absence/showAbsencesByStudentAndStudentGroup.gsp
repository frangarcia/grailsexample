
<%@ page import="com.odobo.eschool.Absence" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'absence.label', default: 'Absence')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-absence" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create" id="${studentGroupInstance.id}"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-absence" class="content scaffold-list" role="main">
			<h1><g:message code="default.listAbsencesByCourse.label" args="[studentGroupInstance]" default="List absences in {0}" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
            <div style="padding-left:26px;">
                <g:formRemote name="search" url="[controller:'absence', action:'search']" update="absenceList">
                    <label for="searchName">Search</label>
                    <g:textField id="searchName" name="searchName"/>
                    <g:textField id="searchSurnames" name="searchSurnames"/>
                    <g:hiddenField name="studentGroupInstance.id" value="${studentGroupInstance.id}"/>
                    <g:submitButton name="searchButton" value="Search"/>
                </g:formRemote>
            </div>
            <div id="absenceList">
                <g:each in="${studentGroupInstance.students}" var="studentInstance">
                    <h1>${studentInstance}</h1>
                    <table>
                        <thead>
                            <tr>

                                <th>${message(code: 'absence.date.label', default: 'Date')}</th>

                                <th>${message(code: 'absence.justifiableCause.label', default: 'Justifiable Cause')}</th>

                                <th>Actions</th>

                            </tr>
                        </thead>
                        <tbody>
                        <g:each in="${studentInstance.getAbsencesByCourse(studentGroupInstance.course)}" status="i" var="absenceInstance">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td><g:formatDate date="${absenceInstance.date}" format="MM/dd/yyyy"/></td>

                                <td>${absenceInstance.justifiableCause ? 'Yes': 'No'}</td>

                                <td>
                                    <g:remoteLink action="changeJustifiable"
                                                  id="${absenceInstance.id}"
                                                  update="absenceList"
                                                  params="[studentGroupId:studentGroupInstance.id]">${absenceInstance.justifiableCause ? 'Mark as unjustifiable' : 'Mark as justifiable' }</g:remoteLink>
                                    | <g:remoteLink action="delete"
                                                    id="${absenceInstance.id}"
                                                    update="absenceList"
                                                    params="[studentGroupId:studentGroupInstance.id]">Delete</g:remoteLink>

                                </td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:each>
            </div>
		</div>
	</body>
</html>

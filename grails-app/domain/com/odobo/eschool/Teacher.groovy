package com.odobo.eschool

class Teacher extends UserProfile {

    static constraints = {
    }

    Boolean teachesStudentGroup(StudentGroup studentGroup){
        StudentGroupTeacher.findByStudentGroupAndTeacher(studentGroup, this) ? true : false
    }

    def getStudentGroups(){
        StudentGroupTeacher.findAllByTeacher(this).studentGroup
    }

    def amITutor(StudentGroup studentGroup){
        StudentGroupTeacher.findAllWhere(studentGroup: studentGroup, teacher: this, tutor: true).size() > 0
    }

}

package com.odobo.eschool

class Absence {

    Date date
    Boolean justifiableCause = false

    static belongsTo = [course:Course, student:Student]

    static constraints = {
        date(nullable: false)
    }
}

package com.odobo.eschool

class Course {

    String name

    static belongsTo = [studyYear:StudyYear]

    static hasMany = [studentGroups:StudentGroup, grades:Grade, absences:Absence]

    static constraints = {
        name(blank:false)
    }

    String toString(){
        "${name} (${studyYear})"
    }
}

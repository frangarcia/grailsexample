package com.odobo.eschool

class UserProfile extends User {

    String name
    String surnames

    static constraints = {
        name(blank:false)
        surnames(blank: false)
    }

    String toString(){
        "${surnames}, ${name}"
    }
}

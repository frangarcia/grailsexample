package com.odobo.eschool

class Grade {

    Float grade
    Course course
    Semester semester

    static belongsTo = [student:Student]

    static constraints = {
        grade(min: 0.0f, max: 10.0f)
    }

    String toString(){
        "${grade} [${course}]"
    }

    static Float averageGradeByCourseAndStudent(Course course, Student student, Semester semester = null){
        def total = 0, grades = 0
        def gradesInstanceList = semester ? Grade.findAllWhere(course:course, student:student, semester: semester) : Grade.findAllWhere(course:course, student:student)
        gradesInstanceList.each { gi ->
            total += gi.grade
            grades++
        }
        grades ? total/grades : 0
    }
}

enum Semester {
    FIRST_SEMESTER, SECOND_SEMESTER
}
package com.odobo.eschool

class Student extends UserProfile {

    static hasMany = [studentGroups:StudentGroup, absences:Absence]
    static belongsTo = [StudentGroup]

    static constraints = {
    }

    Boolean belongsToStudentGroup(StudentGroup studentGroup){
        this.studentGroups.any { it ==  studentGroup }
    }

    def getAbsencesByCourse(Course course){
        Absence.findAllByCourseAndStudent(course, this)
    }

    def getGradesByCourse(Course course){
        Grade.findAllByCourseAndStudent(course, this)
    }
}

package com.odobo.eschool

class StudentGroup {

    String name

    static belongsTo = [course:Course]

    static hasMany = [students:Student]

    static constraints = {
        name(blank:false)
    }

    String toString(){
        "${name} ${course}"
    }

    def getTeachers(){
        def teachers = []
        StudentGroupTeacher.findAllByStudentGroup(this).each { sgt ->
            teachers.add(sgt.teacher)
        }

        teachers
    }

    def searchStudents(String name, String surnames){
        this.students.findAll { it.name.toLowerCase() == name.toLowerCase() || it.surnames.toLowerCase() == surnames.toLowerCase() }
    }
}

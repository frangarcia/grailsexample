package com.odobo.eschool

class StudyYear {

    String name

    static hasMany = [courses:Course]

    static constraints = {
        name(blank: false, unique: true)
    }

    String toString(){
        name
    }
}

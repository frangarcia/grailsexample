package com.odobo.eschool

import org.springframework.dao.DataIntegrityViolationException

class GradeController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def create(Long id) {
        [gradeInstance: new Grade(params), studentGroupInstance:StudentGroup.get(id)]
    }

    def save() {
        def gradeInstance = new Grade(params)
        if (!gradeInstance.save(flush: true)) {
            render(view: "create", model: [gradeInstance: gradeInstance, studentGroupInstance:StudentGroup.get(params.studentGroupInstance.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'grade.label', default: 'Grade'), gradeInstance.id])
        redirect(action: "show", id: gradeInstance.id, params: [studentGroupId:params.studentGroupInstance.id])
    }

    def show(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "showGradesByStudentAndStudentGroup", id:params.studentGroupId)
            return
        }

        [gradeInstance: gradeInstance, studentGroupInstance: StudentGroup.get(params.studentGroupId)]
    }

    def edit(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "showGradesByStudentAndStudentGroup", id:params.studentGroupId)
            return
        }

        [gradeInstance: gradeInstance, studentGroupInstance:StudentGroup.get(params.studentGroupId)]
    }

    def update(Long id, Long version) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "showGradesByStudentAndStudentGroup", params: [id:params.studentGroupInstance.id])
            return
        }

        if (version != null) {
            if (gradeInstance.version > version) {
                gradeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'grade.label', default: 'Grade')] as Object[],
                          "Another user has updated this Grade while you were editing")
                render(view: "edit", model: [gradeInstance: gradeInstance, studentGroupInstance:StudentGroup.get(params.studentGroupInstance.id)])
                return
            }
        }

        gradeInstance.properties = params

        if (!gradeInstance.save(flush: true)) {
            render(view: "edit", model: [gradeInstance: gradeInstance, studentGroupInstance:StudentGroup.get(params.studentGroupInstance.id)])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'grade.label', default: 'Grade'), gradeInstance.id])
        redirect(action: "show", id: gradeInstance.id, params: [studentGroupId:params.studentGroupInstance.id])
    }

    def delete(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "showGradesByStudentAndStudentGroup", id: params.studentGroupId)
            return
        }

        try {
            gradeInstance.delete(flush: true)
            render(template: "listGrades", model: [studentGroupInstance:StudentGroup.get(params.studentGroupId)])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "showAbsencesByStudentAndStudentGroup", id: params.studentGroupId)
        }
    }

    def showGradesByStudentAndStudentGroup(Long id) {
        def user = springSecurityService.currentUser
        def teacherInstance = Teacher.get(user.id)
        def studentGroupInstance = StudentGroup.get(id)
        [studentGroupInstance:studentGroupInstance, amITutor:teacherInstance.amITutor(studentGroupInstance)]
    }

    def beforeInterceptor = [action:this.&beforeSecurityCheck,except:['showGradesByStudentAndStudentGroup', 'update']]

    private beforeSecurityCheck = {
        def user = springSecurityService.currentUser
        def teacherInstance = Teacher.get(user.id)
        def studentGroupInstance = params.studentGroupId ? StudentGroup.get(params.studentGroupId) : StudentGroup.get(params.id)

        if (!teacherInstance.amITutor(studentGroupInstance)){
            flash.message = message(code: 'default.notAllowedAsNotTutor.message', default:"Only tutors can access to this resource")
            redirect(controller: "grade", action:"showGradesByStudentAndStudentGroup", id: params.id)
            return false
        }
    }
}

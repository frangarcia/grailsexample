package com.odobo.eschool

import org.springframework.dao.DataIntegrityViolationException

class AdministratorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [administratorInstanceList: Administrator.list(params), administratorInstanceTotal: Administrator.count()]
    }

    def create() {
        [administratorInstance: new Administrator(params)]
    }

    def save() {
        def administratorInstance = new Administrator(params)
        if (!administratorInstance.save(flush: true)) {
            render(view: "create", model: [administratorInstance: administratorInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'administrator.label', default: 'Administrator'), administratorInstance.id])
        redirect(action: "show", id: administratorInstance.id)
    }

    def show(Long id) {
        def administratorInstance = Administrator.get(id)
        if (!administratorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "list")
            return
        }

        [administratorInstance: administratorInstance]
    }

    def edit(Long id) {
        def administratorInstance = Administrator.get(id)
        if (!administratorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "list")
            return
        }

        [administratorInstance: administratorInstance]
    }

    def update(Long id, Long version) {
        def administratorInstance = Administrator.get(id)
        if (!administratorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (administratorInstance.version > version) {
                administratorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'administrator.label', default: 'Administrator')] as Object[],
                          "Another user has updated this Administrator while you were editing")
                render(view: "edit", model: [administratorInstance: administratorInstance])
                return
            }
        }

        administratorInstance.properties = params

        if (!administratorInstance.save(flush: true)) {
            render(view: "edit", model: [administratorInstance: administratorInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'administrator.label', default: 'Administrator'), administratorInstance.id])
        redirect(action: "show", id: administratorInstance.id)
    }

    def delete(Long id) {
        def administratorInstance = Administrator.get(id)
        if (!administratorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "list")
            return
        }

        try {
            administratorInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'administrator.label', default: 'Administrator'), id])
            redirect(action: "show", id: id)
        }
    }
}

package com.odobo.eschool

import org.springframework.dao.DataIntegrityViolationException

class AbsenceController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def create(Long id) {
        [absenceInstance: new Absence(params), studentGroupInstance:StudentGroup.get(id)]
    }

    def save() {
        def absenceInstance = new Absence(params)
        if (!absenceInstance.save(flush: true)) {
            render(view: "create", model: [absenceInstance: absenceInstance, studentGroupInstance:StudentGroup.get(params.studentGroupInstance.id)])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'absence.label', default: 'Absence'), absenceInstance.id])
        redirect(action: "show", id: absenceInstance.id, params: [studentGroupInstanceId:params.studentGroupInstance.id])
    }

    def show(Long id) {
        def absenceInstance = Absence.get(id)
        if (!absenceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'absence.label', default: 'Absence'), id])
            redirect(action: "list")
            return
        }

        [absenceInstance: absenceInstance, studentGroupInstance: StudentGroup.get(params.studentGroupInstanceId)]
    }

    def delete(Long id) {
        def absenceInstance = Absence.get(id)
        if (!absenceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'absence.label', default: 'Absence'), id])
            redirect(action: "showAbsencesByStudentAndStudentGroup", id: params.studentGroupId)
            return
        }

        try {
            absenceInstance.delete(flush: true)
            render(template: "listAbsences", model: [studentGroupInstance:StudentGroup.get(params.studentGroupId)])
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'absence.label', default: 'Absence'), id])
            redirect(action: "showAbsencesByStudentAndStudentGroup", id: id, params: [studentGroupId:params.studentGroupId])
        }
    }

    def showAbsencesByStudentAndStudentGroup(Long id) {
        [studentGroupInstance:StudentGroup.get(id)]
    }

    def changeJustifiable(Long id) {
        def absenceInstance  = Absence.get(id)
        absenceInstance.justifiableCause = absenceInstance.justifiableCause ? false : true
        absenceInstance.save(flush:true)

        render(template: "listAbsences", model: [studentGroupInstance:StudentGroup.get(params.studentGroupId)])
    }

    def search(){
        def studentGroupInstance = StudentGroup.get(params.studentGroupInstance.id)
        def studentInstanceList = studentGroupInstance.searchStudents(params.searchName, params.searchSurnames)

        render(template:"searchAbsences", model:[studentInstanceList: studentInstanceList, studentGroupInstance:studentGroupInstance])
    }
}

package com.odobo.eschool

import org.springframework.dao.DataIntegrityViolationException

class CourseController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [courseInstanceList: Course.list(params), courseInstanceTotal: Course.count()]
    }

    def create() {
        [courseInstance: new Course(params)]
    }

    def save() {
        def courseInstance = new Course(params)
        if (!courseInstance.save(flush: true)) {
            render(view: "create", model: [courseInstance: courseInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'course.label', default: 'Course'), courseInstance.id])
        redirect(action: "show", id: courseInstance.id)
    }

    def show(Long id) {
        def courseInstance = Course.get(id)
        if (!courseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "list")
            return
        }

        [courseInstance: courseInstance]
    }

    def edit(Long id) {
        def courseInstance = Course.get(id)
        if (!courseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "list")
            return
        }

        [courseInstance: courseInstance]
    }

    def update(Long id, Long version) {
        def courseInstance = Course.get(id)
        if (!courseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (courseInstance.version > version) {
                courseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'course.label', default: 'Course')] as Object[],
                          "Another user has updated this Course while you were editing")
                render(view: "edit", model: [courseInstance: courseInstance])
                return
            }
        }

        courseInstance.properties = params

        if (!courseInstance.save(flush: true)) {
            render(view: "edit", model: [courseInstance: courseInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'course.label', default: 'Course'), courseInstance.id])
        redirect(action: "show", id: courseInstance.id)
    }

    def delete(Long id) {
        def courseInstance = Course.get(id)
        if (!courseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "list")
            return
        }

        try {
            courseInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'course.label', default: 'Course'), id])
            redirect(action: "show", id: id)
        }
    }

    def showStudentGroups(Long id){
        [courseInstance: Course.get(id)]
    }

    def myCoursesAsStudent(){
        def user = springSecurityService.currentUser
        def studentInstance = Student.get(user.id)
        def courseInstanceList = []
        studentInstance.studentGroups.each { sg ->
            courseInstanceList.add(sg.course)
        }

        [courseInstanceList: courseInstanceList]
    }

    def absences(Long id){
        def courseInstance = Course.get(id)
        def user = springSecurityService.currentUser
        def studentInstance = Student.get(user.id)

        [courseInstance: courseInstance, absenceInstanceList:Absence.findAllByCourseAndStudent(courseInstance, studentInstance, [sort:'date', order:'asc'])]
    }

    def grades(Long id){
        def courseInstance = Course.get(id)
        def user = springSecurityService.currentUser
        def studentInstance = Student.get(user.id)
        def gradeFirstSemesterInstanceList = Grade.findAllWhere(course:courseInstance, student:studentInstance, semester:Semester.FIRST_SEMESTER, [sort:'gradeType', order:'asc'])
        def gradeSecondSemesterInstanceList = Grade.findAllWhere(course:courseInstance, student:studentInstance, semester:Semester.SECOND_SEMESTER, [sort:'gradeType', order:'asc'])
        def dataChart = []
        gradeFirstSemesterInstanceList.each { g->
            dataChart.add(['FIRST SEMESTER',g.grade])
        }
        gradeSecondSemesterInstanceList.each { g->
            dataChart.add(['SECOND SEMESTER',g.grade])
        }


        [courseInstance: courseInstance,
                gradeFirstSemesterInstanceList:gradeFirstSemesterInstanceList,
                averageGradeFirstSemesterByCourseAndStudent:Grade.averageGradeByCourseAndStudent(courseInstance, studentInstance, Semester.FIRST_SEMESTER),
                gradeSecondSemesterInstanceList:gradeSecondSemesterInstanceList,
                averageGradeSecondSemesterByCourseAndStudent:Grade.averageGradeByCourseAndStudent(courseInstance, studentInstance, Semester.SECOND_SEMESTER),
                averageGradeByCourseAndStudent:Grade.averageGradeByCourseAndStudent(courseInstance, studentInstance),
                dataChart:dataChart
        ]
    }

    def myCoursesAsTeacher(){
        def user = springSecurityService.currentUser
        def teacherInstance = Teacher.get(user.id)
        def studentGroupInstanceList = teacherInstance.studentGroups

        [studentGroupInstanceList: studentGroupInstanceList, teacherInstance:teacherInstance]
    }
}
